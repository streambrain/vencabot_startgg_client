# startgg_client_vencabot
## Description
This module provides a simple Python interface for interacting with Start.gg.

## Status
As of right now, this module only has two functions: one for getting all of the Events in a League and one for getting the Standings of a League. They're both probably bugged because they're only capable of fetching one page. Since a page can hold 500 items, that's not the end-of-the-world in most cases, but it's not really 'production ready' in that respect.

## License
This module is licensed under the [GNU General Public License v3.0 or later](https://www.gnu.org/licenses/gpl-3.0-standalone.html).
